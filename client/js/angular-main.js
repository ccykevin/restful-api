/* global angular */

var app = angular.module('app', ['ngRoute', 'ngCookies']);

app.factory('TemporaryStorage', function() {
    var artistObject = {
        artist: "",
        albums: ""
    };
    
    var albumObject = {
        tracks: "",
        reviews: ""
    };
    
    var userObject = {
        username: "",
        userID:   ""
    }
    
    return {
        setArtist: function(artist) {
            artistObject.artist = artist;
        },
        setAlbums: function(albums) {
            artistObject.albums = albums;
        },
        getArtistObject: function() {
            return artistObject;
        },
        setTracks: function(tracks) {
            albumObject.tracks = tracks;
        },
        setReviews: function(reviews) {
            albumObject.reviews = reviews;
        },
        getAlbumObject: function() {
            return albumObject;
        },
        setUsername: function(username) {
            albumObject.username = username;
        },
        setUserID: function(userID) {
            albumObject.userID = userID;
        },
        getUserObject: function() {
            return userObject;
        }
        
    };
});

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/search', {
            templateUrl: 'templates/search.html',
            controller: 'printAlbums'
        })
        .when('/albumDetail/:mbid', {
            templateUrl: 'templates/albumDetail.html',
            controller: 'printDetail'
        })
        .otherwise({
            redirectTo: '/search'
        });
}]);

app.controller('searchArtist', function($scope, $http, TemporaryStorage, $cookies, $window) {
    var checking = $cookies.get('userID') == undefined ? false : true;
    
    $scope.loginStatus = checking;
    $scope.showLogin   = !checking;
    
    if (checking) {
        $scope.loggedUsername = $cookies.get('username');
        $scope.loggedUserID   = $cookies.get('userID');
        TemporaryStorage.setUsername($cookies.get('username'));
        TemporaryStorage.setUserID($cookies.get('userID'));
    }

    $scope.submit = function($event) {
        $scope.loading = true;
        if ($scope.artist != undefined && $scope.artist != "") {
            $http.get('https://assignment-restful-api.herokuapp.com/api/searchArtist?name=' + $scope.artist)
                .then(function(response) {
                    if (response.status == 200) {
                        TemporaryStorage.setArtist(response.data.artist);
                        TemporaryStorage.setAlbums(response.data.albums);
                    }
                }).finally(function () {
                    // Hide loading spinner whether our call succeeded or failed.
                    $scope.loading = false;
                });
        }
    };
    
    $scope.logout = function($event) {
        $cookies.remove('username');
        $cookies.remove('userID');
        $window.location = "index.html";
    };
});

app.controller('printAlbums', function($scope, $http, TemporaryStorage) {
     $scope.$watchCollection(function () {
        return [TemporaryStorage.getArtistObject().albums, TemporaryStorage.getArtistObject().artist];
     }, function (newVal, oldVal) {
        if (newVal != oldVal) {
            $scope.albums = newVal[0];
            $scope.artist = newVal[1];
        }
    });
});

app.controller('printDetail', function($scope, $http, $routeParams, $cookies, $window) {
    var mbid = $routeParams.mbid;
    $scope.loading = true;
    
    $http.get('https://assignment-restful-api.herokuapp.com/api/album/' + mbid)
        .then(function(response) {
            if (response.status == 200) {
                $scope.tracks   = response.data.album.tracks;
                $scope.reviews = response.data.reviews;
                $scope.loggedUsername = $cookies.get('username');
                $scope.loggedUserID   = $cookies.get('userID');
            }
        }).finally(function () {
            // Hide loading spinner whether our call succeeded or failed.
            $scope.loading = false;
        });
        
    $scope.submitNewReview = function($event) {
        var userName = $scope.loggedUsername;
        var review   = $scope.review;
        
        var existing = false;
        var reviews = document.getElementsByClassName("reviewRow-userName");
        var existingID = "";
        var reviewElement = "";
        
        for (var i = 0; i < reviews.length; i++) {
            if (reviews[i].innerHTML == userName) {
                existingID = reviews[i].closest(".reviewRow").getAttribute("data-reviewid");
                reviewElement = reviews[i].closest(".reviewRow");
                existing = true;
            }
        }
        
        if (!existing) {
            var data = {
                review   : review,
                mbid     : mbid,
                userName : userName
            };
            
            $http.post("https://assignment-restful-api.herokuapp.com/api/album", data).then(function(response) {
                $scope.review = null;
                var newView = `<div class="col-sm-12 reviewRow" data-reviewid="`+ response.data.id +`">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <strong class="reviewRow-userName">`+ userName +`</strong>
                                    </div>
                                    <div class="panel-body">
                                        `+ review +`
                                    </div>
                                </div>
                            </div>`
                
                angular.element(document.querySelector( '#reviewsArea' )).append(newView);
            }, function(error) {
                console.log(error);
            });
            
            // $http({
            //     method: 'POST',
            //     url: "https://assignment-restful-api.herokuapp.com/api/album",
            //     headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            //     transformRequest: function(obj) {
            //         var str = [];
            //         for(var p in obj)
            //         str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            //         return str.join("&");
            //     },
            //     data: data
            // }).then(function(response) {
            //     console.log(response)
            // }, function(error) {
            //     console.log(error)
            // });
        } else {
            var updateReviewObject = {
                "review": review
            }
            
            $http.put("https://assignment-restful-api.herokuapp.com/api/album/"+mbid+"/"+existingID, updateReviewObject).then(function(response) {
                if (response.status == 200) {
                    var responseData = response.data;
                    
                    if (responseData.successful == undefined){
                        $window.location.reload();
                    } else {
                        $window.alert(responseData.message);
                    }
                }
            }, function(error) {
                console.log(error);
            });
        }
    };
    
    $scope.deleteReview = function(reviewID) {
        var sureDelete = $window.confirm('Are you absolutely sure you want to delete?');

        if (sureDelete) {
            $http.delete("https://assignment-restful-api.herokuapp.com/api/album/"+mbid+"/"+reviewID, {}).then(function(response) {
                if (response.status == 200) {
                    var responseData = response.data;
                    
                    if (responseData.successful == undefined){
                        angular.element(document.querySelector('div[data-reviewid="'+reviewID+'"]')).remove();
                    } else {
                        $window.alert(responseData.message);
                    }
                }
            }, function(error) {
                console.log(error);
            });
        }
    };
});

app.controller('registerController', function($scope, $http, $routeParams, $window) {
    $scope.register = function($event) {
        var data = {
            username : $scope.username,
            password : $scope.password,
            email    : $scope.email
        };
        
        $http.post("https://assignment-restful-api.herokuapp.com/api/user", data).then(function(response) {
            if (response.status == 200) {
                var data = response.data;
                
                if (data.successful == undefined){
                    $window.alert("Registration Successful");
                    $window.location = "login.html";
                } else {
                    $window.alert(data.message);
                }
            }
        }, function(error) {
            console.log(error);
        });
    };
});

app.controller('loginController', function($scope, $http, $routeParams, $window, $cookies) {
    var checking = $cookies.get('userID') == undefined ? false : true;
    
    if (checking) {
        $window.location = "index.html";
    }
    
    $scope.login = function($event) {
        var data = {
            username : $scope.loginName,
            password : $scope.loginPassword
        };

        $http.post("https://assignment-restful-api.herokuapp.com/api/login", data).then(function(response) {
            if (response.status == 200) {
                var responseData = response.data;
                
                if (responseData.successful == undefined){
                    $cookies.put("username", responseData.username);
                    $cookies.put("userID", responseData.userID);
                    $window.alert("Login Successful");
                    $window.location = "index.html";
                } else {
                    $window.alert(responseData.message);
                }
            }
        }, function(error) {
            console.log(error);
        });
    };
});