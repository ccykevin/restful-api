// Import modules
var restify      = require('restify');
var server       = restify.createServer();
var apis         = require("./api");

// Configure Server
server.use(restify.fullResponse());
server.use(restify.bodyParser());
server.use(restify.CORS({
    credentials: true
}));

// {
//   origins: ['*'],
//   credentials: false, // defaults to false
//   headers: ['']  // sets expose-headers
// }


// Test server port
var port = process.env.PORT || 3000;
server.listen(port, function (err) {
    if (err) {
        console.error(err);
    } else {
        console.log('App is ready at: '+ port);
    }
});

// APIs
var handleSearchArtist   = apis.handleSearchArtist;
var handleNewComment     = apis.handleNewComment;
var handleSearchAlbum    = apis.handleSearchAlbum;
var handleOptionsRequest = apis.handleOptionsRequest;
var handleDeleteReview   = apis.handleDeleteReview;
var handleUpdateReview   = apis.handleUpdateReview;
var handleNewUser        = apis.handleNewUser;
var handleLogin          = apis.handleLogin;

// Search artist
server.get("/api/searchArtist", handleSearchArtist);
//
// server.opts('/api/album', handleOptionsRequest);
// Create New Review
server.post("/api/album", handleNewComment);
// Query album detail and review
server.get("/api/album/:mbid", handleSearchAlbum);
// Handle OPTIONS Request
server.opts('/api/album/:mbid/:reviewID', handleOptionsRequest);
// Delete review
server.del("/api/album/:mbid/:reviewID", handleDeleteReview);
// Update review
server.put("/api/album/:mbid/:reviewID", handleUpdateReview);


// Create User
server.post("/api/user", handleNewUser);
// Login
server.post("/api/login", handleLogin);