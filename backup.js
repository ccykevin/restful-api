// Import modules
var restify      = require('restify');
var server       = restify.createServer();
var asyncRequest = require('request');
var syncRequest  = require('sync-request');
var firebase     = require('firebase');
var urlHandler   = require('url');

// Configure Server
server.use(restify.fullResponse());
server.use(restify.bodyParser());

// Test server port
var port = process.env.PORT || 3000;
server.listen(port, function (err) {
    if (err)
        console.error(err);
    else
        console.log('App is ready at: '+ port);
});

// Initialize the Firebase with a service account, granting admin privileges
firebase.initializeApp({
  databaseURL: "https://assignment-part2n3.firebaseio.com",
  serviceAccount: "Assignment-Part23-c4decf3cbc8d.json"
});
var reviewRef = firebase.database().ref("/review");

// API URL and key
var lastFmURL    = 'http://ws.audioscrobbler.com/2.0/';
var lastFmAPIKey = 'd5adeaee26ea9f9621f4be79ce14950b';
var resultFormat = 'json';

// Youtube URL & API KEY
var youtubeURL             = 'https://www.googleapis.com/youtube/v3/search';
var youtubeAPIKey          = 'AIzaSyB1r0jX4KJj7Hw9NdzsEigrxRbv-Obd3Uw';
var youtubeSearchPart      = 'snippet';
var youtubeSearchType      = 'video';
var youtubeSearchOrder     = 'relevance';
var youtubeSearchMaxResult = '1';

server.get("/api/searchArtist", handleSearchArtist);

// Create New Review
server.post("/api/album", handleNewComment);
// Query album detail and review
server.get("/api/album/:mbid", handleSearchAlbum);
// Handle OPTIONS Request
server.opts('/api/album/:mbid/:reviewID', handleOptionsRequest);
// Delete review
server.del("/api/album/:mbid/:reviewID", handleDeleteReview);
// Update review
server.put("/api/album/:mbid/:reviewID", handleUpdateReview);

// Handler
function handleOptionsRequest (request, response) {
    response.send(200);
}

function handleSearchArtist(request, response) {
    var url = urlHandler.parse(request.url, true).query;
    response.setHeader('content-type', 'application/json');
    response.charSet('utf-8');
    
    // Artist name cannot empty
    if (url.name != undefined && url.name != "" ) {
        queryArtishInfo(url.name, queryTopAlbums);
    } else {
        response.send(404,
        {   
            "successful" : "false",
            "message"    : "Artist name cannot empty"
        });
        response.end();
    }
    
    // Query top albums of that artist
    function queryTopAlbums(artistName, artistImage){
        if (artistName != "") {
            var queryString = {
                method: 'artist.getTopAlbums', 
                artist: artistName, 
                api_key: lastFmAPIKey, 
                format: resultFormat
            };
            processAlbums(artistName, artistImage, queryString, outputJSON);
        } else {
            response.send(404,
            {   
                "successful" : "false",
                "message"    : "Artist Not Found"
            });
            response.end();
        }
    }
    
    // Output JSON
    function outputJSON (artistName, artistImage, albums) {
        response.send(200,
        {   
            "artist":
            {
                "name" : artistName,
                "image": artistImage
            },
            "albums"     : albums,
            "successful" : "true"
        });
        response.end();
    }
}

function handleSearchAlbum (request, response) {
    var mbid = request.params.mbid;
    response.setHeader('content-type', 'application/json');
    response.charSet('utf-8');
    
    // mbid cannot empty
    if (mbid != undefined && mbid != "" ) {
        queryAlbumDetail(mbid, outputJSON);
    } else {
        response.send(404,
        {   
            "successful" : "false",
            "message"    : "mbid cannot empty"
        });
        response.end();
    }
    
    // Output JSON
    function outputJSON (artist, albumName, tracksArray, reviews) {
        if (artist != "" && albumName != "" && tracksArray.length != 0) {
            
            response.send(200,
                {   
                    "artist"     : artist,
                    "album"      :
                    {
                        "name"       : albumName,
                        "tracks"     : tracksArray
                    },
                    "reviews"     : reviews,
                    "successful" : "true"
            });
        } else {
            response.send(404,
            {   
                "successful" : "false",
                "message"    : "Empty Tracks"
            });
        }
        response.end();
    }
}

function handleNewComment (request, response) {
    response.setHeader('content-type', 'application/json');
    response.charSet('utf-8');

    var mbid   = request.params.mbid;
    var review = request.params.review;
    var userID = request.params.userID;
    
    if (mbid != undefined && mbid != "" && review != undefined && review != "" && userID != undefined && userID != "" ) {
        firebase.database().ref("/review/" + mbid + "/").push().set({
            "userID" : userID,
            "review" : review
        });
    
        response.send(200, {});
        response.end();
    } else {
        response.send(400,
            {   
                "successful" : "false",
                "message"    : "Missing required value"
            });
        response.end();
    }
}

function handleDeleteReview (request, response) {
    response.setHeader('content-type', 'application/json');
    response.charSet('utf-8');
    
    var mbid     = request.params.mbid;
    var reviewID = request.params.reviewID;

    reviewRef.child(mbid + "/"+ reviewID).once("value", function(snapshot) {
        // Existing Record
        var counter = 0;
        snapshot.forEach(function(data) {
            counter++;
        });
    
        if (counter > 0){
            reviewRef.child(mbid + "/"+ reviewID).remove();
            response.send(200, {});
            response.end();
        } else {
            // Error Handle
            response.send(400, {
                "successful": false,
                "message"   : "Not existing record."
            });
            response.end();
        }
        
    }, function(err) {
        // Error Handle
        response.send(400, {
            "successful" : false,
            "message"    : err
        });
        response.end();
    });
}

function handleUpdateReview (request, response) {
    response.setHeader('content-type', 'application/json');
    response.charSet('utf-8');
    
    var mbid     = request.context.mbid;
    var reviewID = request.context.reviewID;
    var review   = request.params;
    
    reviewRef.child(mbid + "/"+ reviewID).once("value", function(snapshot) {
        // Existing Record
        var counter = 0;
        snapshot.forEach(function(data) {
            counter++;
        });
    
        if (counter > 0){
            reviewRef.child(mbid + "/"+ reviewID).update({
                "review" : review
            });
            response.send(200, {});
            response.end();
        } else {
            // Error Handle
            response.send(400, {
                "successful": false,
                "message"   : "Not existing record."
            });
            response.end();
        }
        
    }, function(err) {
        // Error Handle
        response.send(400, {
            "successful" : false,
            "message"    : err
        });
        response.end();
    });
}

//Query artist information
function queryArtishInfo(name, callback) {
    var query_string = {method: 'artist.getInfo', artist: name, api_key: lastFmAPIKey, format: resultFormat};
    
    asyncRequest.get(
        {
            url: lastFmURL,
            qs: query_string
        },
        function (error, response, body){
            if (error) { // Error Handle
                console.log(error);
            } else {
                var jsonObject = JSON.parse(body);
                
                if ( jsonObject.error != 6 ) { // If last.fm do not return error code
                    var artistName  = jsonObject['artist']['name'];
                    var artistImage = jsonObject['artist']['image'][2]['#text'];
                    callback(artistName, artistImage);
                    
                } else {
                    callback("", "");
                }
            }
        }
    );
}

// Query albums detail
function processAlbums(artistName, artistImage, queryString, callback){
    asyncRequest.get(
        {
            url: lastFmURL, 
            qs: queryString
        }, 
        function(error, response, body) {
            if (error) { // Error Handle
                console.log(error);
            } else {
                var jsonObject  = JSON.parse(body);
                var albumsArray = [];
                
                if ( jsonObject.error != 6 ) { // If last.fm do not return error code
                    var albums = jsonObject['topalbums']['album'];
                    
                    for ( var i = 0; i < albums.length; i++ ) {
                        var albumName  = albums[i]['name'];
                        var albumMBID  = albums[i]['mbid'];
                        var albumImage = albums[i]['image'][2]['#text'];
        
                        if ( albumMBID != undefined ) {
                            albumsArray.push({
                                "name" : albumName,
                                "mbid" : albumMBID,
                                "image": albumImage
                            });
                        }
                    }
                }
                
                callback(artistName, artistImage, albumsArray);
            } 
        }
    );
}

// Query Youtube Link
function queryAlbumDetail(mbid, callback){
    var query_string = {method: 'album.getInfo', mbid: mbid, api_key: lastFmAPIKey, format: resultFormat};
    
    asyncRequest.get(
        {
            url: lastFmURL, 
            qs: query_string
        },
        function (error, response, body) {
            if (error) { // Error Handle
                console.log(error);
            } else {
                var jsonObject = JSON.parse(body);
                var tracksArray = [];
                if ( jsonObject.error != 6 ) { // If last.fm do not return error code
                    var artist    = jsonObject['album']['artist'];
                    var albumName = jsonObject['album']['name'];
                    var tracks    = jsonObject['album']['tracks']['track'];
                    
                    for ( var i = 0; i < tracks.length; i++ ){
                        var queryString = artist +" "+ tracks[i]['name'];
                        
                        var res = syncRequest('GET', youtubeURL, {
                          'qs': {
                              'key'       : youtubeAPIKey,
                              'part'      : youtubeSearchPart,
                              'q'         : queryString,
                              'type'      : youtubeSearchType,
                              'order'     : youtubeSearchOrder,
                              'maxResults': youtubeSearchMaxResult
                          } 
                        });
                        
                        var youtubeSearchResult = JSON.parse(res.getBody('utf8'));
                        var youtubeVideoID      = youtubeSearchResult['items'][0]['id']['videoId'];
                        
                        tracksArray.push({
                            "name" : tracks[i]['name'],
                            "link" : "https://www.youtube.com/watch?v="+ youtubeVideoID
                        });
                    }
                    
                    var reviews = [];
            
                    reviewRef.child(mbid).once("value", function(snapshot) {
                        snapshot.forEach( function(rawData){
                            var data = rawData.val();
                            reviews.push({
                                "userID" : data.userID,
                                "review" : data.review
                            });
                        });
                        
                        callback(artist, albumName, tracksArray, reviews);
                    });
                } else { // Error Handle for album not found
                    callback("", "", [], []);
                }
            }
        }
    );
}